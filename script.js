/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?{

    Реалізувати це можна за допомоги івентів keydown та keyup
}
    
2. Яка різниця між event.code() та event.key()?{

    event.key() - повертає ту клавішу яку було натиснутно з урахуванням розкладки або мови
    (тобто якщо користувач натисне "ї" консоль виведе "ї")

    event.code() - повератє фізичне місце клавіші без урахування розкладки або мови
    (тобто якщо користувач натисне "ї" консоль виведе "BracketRight")
}

3. Які три події клавіатури існує та яка між ними відмінність?{

    keydown, keypress та keyup.

    keydown - евент реагує саме на натискання клавіші, та може викликатисть повторно
    при утримуванні клавіші

    keyup - евент реагує коли користувач відпускає клавішу

    keypress - реагує саме на натискання клавіші. Також ураховує розкладку і виводить саме символ.
    Цей евент застарів і його не рекомендують до використання
}

Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
Але якщо при натискані на кнопку її  не існує в розмітці, то попередня активна кнопка повина стати неактивною.
*/

const btn = document.querySelectorAll('.btn');

window.addEventListener('keydown', event => {

    btn.forEach(function (btn) {

        if (btn.textContent === event.code.replace('Key', '')) {
            btn.style.backgroundColor = 'blue';
        }
    });
});

window.addEventListener('keyup', event => {

    btn.forEach(function (btn) {

        if (btn.textContent === event.code.replace('Key', '')) {
            btn.style.backgroundColor = '';
        }
    });
});

